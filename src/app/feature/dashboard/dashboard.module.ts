import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { VotersComponent } from './voters/voters.component';
import { CoreModule } from 'src/app/core/core.module';

@NgModule({
  declarations: [VotersComponent],
  imports: [
    CoreModule,
    CommonModule,
    DashboardRoutingModule
  ]
})
export class DashboardModule { }
