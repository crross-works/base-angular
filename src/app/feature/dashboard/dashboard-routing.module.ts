import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VotersComponent } from './voters/voters.component';

const routes: Routes = [
  {
    path: "dashboard/voter",
    component: VotersComponent
  },
  {
    path: 'dashboard',
    redirectTo: '/dashboard/voter',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
